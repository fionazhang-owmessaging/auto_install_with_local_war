#! /bin/bash

if [[ ! -f ~/webtop_auto-install/webtop_config ]]; then
	echo "config file doesn't exist, please check it manually."
	exit 1
fi

#if [[ $# != 1 ]]; then
#	exit 1
#fi

user=$(whoami)
if [[ $user == "root" ]]; then
	echo "Please DO NOT run this tool as ROOT"
	exit 1
fi

# check the URL is valid
#wget --spider -nv $1
#if [[ $? != 0 ]]; then
#	echo "Please input the war name"
#	exit 1
#fi

source ./webtop_config

# shutdown tomcat
$tmDir/bin/shutdown.sh >/dev/null 2>&1
kill -9 $(ps -ef | grep tomcat | grep -v grep | awk '{print $2}')

# remove old link directory
#rm -rf $tmDir/webapps/kiwi-master

# create build directory and download build into it
#if [[ ! -d $buildDir ]]; then
#	mkdir -p $buildDir
#fi

echo '**********************************************'
echo 'check latest build'
echo '**********************************************'

#wget -P $buildDir $1 >$buildDir/download.log 2>&1 
#if [[ $? != 0 ]]; then
#	echo "download build failed, please check the given URL"
#	rm -rf $buildDir
#	exit 1
#fi

# create a directory for new build 
#war=$(cat $buildDir/download.log | grep saved | awk -F \` '{print $2}' | awk -F \' '{print $1}' | awk -F / '{print $NF}')
#version=$(echo ${war%.*} | awk -F 'se-' '{print $NF}')
#appDir=$tmDir/webapps/kiwi-octane_$version

# get the latest war
new_war=$(ls -ltr $buildDir*.war | awk -F '/' {'print $NF'} | tail -1 )
version=$(echo ${new_war%.*} | awk -F 'se-' {'print $NF'})
appDir=~/webtop/webapps/$new_war_$version

echo " the latest build number is: ${new_war}  "

if [[ -d $appDir ]]; then
	rm -rf $appDir
fi

mkdir -p $appDir

echo '**********************************************'
echo 'unzip war'
echo '**********************************************'
unzip $buildDir$new_war -d $appDir >>/dev/null 2>&1 
if [[ $? != 0 ]]; then
	echo "unzip build failed, please check it manually"
	#rm -rf $buildDir
	rm -rf $appDir
	exit 1
fi

echo '**********************************************'
echo 'backup and make a new config'
echo '**********************************************'
cp $appDir/WEB-INF/classes/config/webtop-config.xml $appDir/WEB-INF/classes/config/webtop-config_backup.xml

sed -i "s#mxos url=\"http://.*:*/mxos\"#mxos url=\"http://${mxosHost}:${mxosPort}/mxos\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#service uri=\"http://.*:.*/webtop-media\"#service uri=\"http://${webtopHost}:${webtopPort}/webtop-media\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#store protocol=\"imap\" host=\".*\" port=\".*\"#store protocol=\"imap\" host=\"${imapHost}\" port=\"${imapPort}\" starttls=\"false\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#transport protocol=\"smtp\" host=\".*\" port=\".*\"#transport protocol=\"smtp\" host=\"${smtpHost}\" port=\"${smtpPort}\" auth=\"false\" starttls=\"false\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#calDavServer host=\".*\" port=\".*\" webappContext#calDavServer host=\"${calHost}\" port=\"${calPort}\" webappContext#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#rsvp url=\"http://.*:8080/kiwi-master.*\"#rsvp url=\"http://${webtopHost}:${webtopPort}/kiwi-master\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#http://.*:8080/kiwi-master.*/http/shareCalendar#http://${webtopHost}:${webtopPort}/kiwi-master/http/shareCalendar#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#userDomain value=\".*\"#userDomain value=\"openwave.com\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#allowCalDomain value=\".*\"#allowCalDomain value=\"openwave.com\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#soft=\"20m\"#soft=\"10h\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#<junk name=\"Junk\"#<junk name=\"Spam\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#<trash name=\"Deleted\"#<trash name=\"Trash\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml
sed -i "s#<pab version=\"8\"#<pab version=\"9\"#g" $appDir/WEB-INF/classes/config/webtop-config.xml

# check whether contactgroups is enabled
typeset contactgroup=$(grep enablecontactgroups $appDir/custom-config.js | awk '{print $NF}' | awk -F , '{print $1}')
if [[ $contactgroup != "false" ]]; then
	echo "enable contact groups"
	sed -i "s#enablecontactgroups: ${contactgroup},#enablecontactgroups: true,#g" $appDir/custom-config.js
fi

# check whether enableGlobalSearch is enabled
typeset enableGlobalSearch=$(grep "enableGlobalSearch:" $appDir/client-config.js | awk '{print $NF}')
if [[ $enableGlobalSearch != "true" ]]; then
	echo "enable Global Search"
	sed -i "s#enableGlobalSearch: ${enableGlobalSearch},#enableGlobalSearch: true,#g" $appDir/client-config.js
fi

chmod 755 $appDir
chown -R imail:imail $appDir

# create new link directory
ln -s $appDir $tmDir/webapps/kiwi-master

# cleanup all mail account
echo '**********************************************'
echo "cleanup all test mail account"
echo '**********************************************'
for ((i=8001; i<8051; i++))
do
	curl -v -X DELETE "http://${mxosHost}:${mxosPort}/mxos/mailbox/v2/autotest${i}@openwave.com?eraseUserAddressBook=true" >/dev/null 2>&1
	curl -v -X PUT "http://${mxosHost}:${mxosPort}/mxos/mailbox/v2/autotest${i}@openwave.com/" -d "cosId=default&password=p" >/dev/null 2>&1
	curl -v -X POST "http://${mxosHost}:${mxosPort}/mxos/mailbox/v2/autotest${i}@openwave.com/mailReceipt/filters/sieveFilters" -d "sieveFilteringEnabled=yes" >/dev/null 2>&1
done

echo '**********************************************'
echo 'restart tomcat'
echo '**********************************************'
$tmDir/bin/startup.sh >/dev/null 2>&1
echo "use the following command to monitor startup"
echo "tail -f $tmDir/logs/catalina.out"
echo '**********************************************'
echo "after that, please access the following URL"
echo "http://${webtopHost}:${webtopPort}/$new_war_$version"
